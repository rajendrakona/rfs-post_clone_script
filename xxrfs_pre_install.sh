SYSTEM_NAME="SYSTEM"
APPS_NAME="APPS"
EFSR_NAME="EFSR"
LOG_FILE="install.log"
CLEANUP_LOG_FILE="cleanup_install.log"

# *******************************************************************
#  Function to Check the login
# *******************************************************************                                                          
CHKLOGIN(){                                                     
    if sqlplus -s /nolog <<! >/dev/null 2>&1 
    set pagesize 0 feedback off verify off heading off echo off
    WHENEVER SQLERROR EXIT 1;
        CONNECT $1 ;
        EXIT;
!
    then 
        echo OK
    else
        echo NOK
    fi
}

CONN_SID=""
while [ "$CONN_SID" = ""  ]
do
    echo "Enter Connect String SID"  | tee -a $LOG_FILE
    read CONN_SID
done         

APPS_PWD=""
while [ "$APPS_PWD" = ""  ]
do
    echo "Enter APPS password"  | tee -a $LOG_FILE
    stty -echo
    read APPS_PWD
    stty echo
done         

# *******************************************************************
#  Check if Login Id is correct else prompt to get it again
# *******************************************************************
while [  `CHKLOGIN "$APPS_NAME/$APPS_PWD@$CONN_SID" "DUAL"` = "NOK"  ]
do
    echo "Password OR Connect String SID may NOT BE CORRECT. Please Re-enter"
    
    CONN_SID=""
    while [ "$CONN_SID" = ""  ]
    do
        echo "Enter Connect String SID"  | tee -a $LOG_FILE
        read CONN_SID
    done         

    APPS_PWD=""
    while [ "$APPS_PWD" = ""  ]
    do
        echo "Enter APPS password"  | tee -a $LOG_FILE
        stty -echo
        read APPS_PWD
        stty echo
    done 
done

EFSR_PWD=""
while [ "$EFSR_PWD" = ""  ]
do
    echo "Enter EFSR password"  | tee -a $LOG_FILE
    stty -echo
    read EFSR_PWD
    stty echo
done         

# *******************************************************************
#  Check if Login Id is correct else prompt to get it again
# *******************************************************************
while [  `CHKLOGIN "$EFSR_NAME/$EFSR_PWD@$CONN_SID" "DUAL"` = "NOK"  ]
do
    echo "Password for EFSR may NOT BE CORRECT. Please Re-enter"
    
    EFSR_PWD=""
    while [ "$EFSR_PWD" = ""  ]
    do
        echo "Enter EFSR password"  | tee -a $LOG_FILE
        stty -echo
        read EFSR_PWD
        stty echo
    done         
done

echo "installing version dev1" | tee -a $LOG_FILE
