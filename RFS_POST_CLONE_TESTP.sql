CREATE OR REPLACE procedure APPS.rfs_post_clone_testp
AS     
    PROCEDURE update_ftp_parameters
    IS
    ln_ftp_param_count   NUMBER :=0;
    BEGIN
        dbms_output.put_line('Executing update_ftp_parameters()');
        
        dbms_output.put_line('Updating APPS_SERVER_BASEDIR of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = '/u001/oracle/TESTP/apps_intf'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'APPS_SERVER_BASEDIR';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_BASEDIR of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = '/u001/oracle/TESTP/db_intf'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'DB_SERVER_BASEDIR';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_HOST of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = '10.60.3.5'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'DB_SERVER_HOST';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_PORT of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = '22'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'DB_SERVER_PORT';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_USER of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = 'oratestp'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'DB_SERVER_USER';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_PROTOCOL of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = 'SFTP'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'FTP_PROTOCOL';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_BASEDIR of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = '/d01/rfs/rfstest'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'FTP_SERVER_BASEDIR';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_HOST of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = '35.160.191.236'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'FTP_SERVER_HOST';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_PORT of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = '22'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'FTP_SERVER_PORT';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_USER of EFSR_FTP_PARAMETERS');
        UPDATE apps.efsr_lookups el
        SET    el.description = 'rfsprd'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_FTP_PARAMETERS'
        AND    el.lookup_code = 'FTP_SERVER_USER';
        ln_ftp_param_count   := ln_ftp_param_count + SQL%ROWCOUNT ;

        COMMIT;
        dbms_output.put_line('Lookup Type - EFSR_FTP_PARAMETERS' || ' Total No Of records updated - ' || ln_ftp_param_count );
        dbms_output.put_line('Updation of EFSR_FTP_PARAMETERS is successfully completed');
        EXCEPTION
        WHEN OTHERS THEN
        ROLLBACK;
        dbms_output.put_line('Exception while updating EFSR_FTP_PARAMETERS');
        dbms_output.put_line('Lookup Type - EFSR_FTP_PARAMETERS' || ' Total No Of records updated - ' || ln_ftp_param_count );
    END update_ftp_parameters;

    PROCEDURE update_xref_rfs_to_ebs_lt
    IS
    ln_rfs_to_ebs_param_count   NUMBER :=0;
    BEGIN
        dbms_output.put_line('Executing update_xref_rfs_to_ebs_lt()');
        
        dbms_output.put_line('Updating APPS_SERVER_BASEDIR of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = '/u001/oracle/TESTP/apps_intf/'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'APPS_SERVER_BASEDIR';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_BASEDIR of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = '/u001/oracle/TESTP/db_intf'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'DB_SERVER_BASEDIR';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_HOST of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = '10.60.3.5'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'DB_SERVER_HOST';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_PORT of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = '22'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'DB_SERVER_PORT';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating DB_SERVER_USER of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = 'oratestp'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'DB_SERVER_USER';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_PROTOCOL of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = 'CUSTOM'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'FTP_PROTOCOL';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_BASEDIR of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = '/ERP/EDR/Test/Alpha'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'FTP_SERVER_BASEDIR';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_HOST of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = 'ftp.rfssales.com'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'FTP_SERVER_HOST';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_PORT of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = '21'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'FTP_SERVER_PORT';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating FTP_SERVER_USER of EFSR_XFER_RFS_TO_EBS_PARAMETER');
        UPDATE apps.efsr_lookups el
        SET    el.description = 'EDR_Effiser'
        WHERE  1=1
        AND    el.lookup_type = 'EFSR_XFER_RFS_TO_EBS_PARAMETER'
        AND    el.lookup_code = 'FTP_SERVER_USER';
        ln_rfs_to_ebs_param_count := ln_rfs_to_ebs_param_count + SQL%ROWCOUNT ;

        COMMIT;
        dbms_output.put_line('Updation of EFSR_XFER_RFS_TO_EBS_PARAMETER is successfully completed');
        dbms_output.put_line('Lookup Type - EFSR_XFER_RFS_TO_EBS_PARAMETER' || ' Total No Of records updated - ' || ln_rfs_to_ebs_param_count );
        EXCEPTION
        WHEN OTHERS THEN
        ROLLBACK;
        dbms_output.put_line('Exception while updating EFSR_XFER_RFS_TO_EBS_PARAMETER');
        dbms_output.put_line('Lookup Type - EFSR_XFER_RFS_TO_EBS_PARAMETER' || ' Total No Of records updated - ' || ln_rfs_to_ebs_param_count );
    END update_xref_rfs_to_ebs_lt;

PROCEDURE update_profile_options
    IS
    ln_profile_option_count   NUMBER :=0;
    BEGIN
        dbms_output.put_line('Executing update_profile_options()');
        
        dbms_output.put_line('Updating Profile Option Value Of Profile - EFSR_TALEND_HOST_ADDR');
        UPDATE apps.fnd_profile_option_values fpov
        SET fpov.profile_option_value = 'http://52.40.176.6:8085' 
         WHERE fpov.profile_option_id = (SELECT fpo.profile_option_id
                                         FROM apps.fnd_profile_options fpo
                                         WHERE fpo.profile_option_name = 'EFSR_TALEND_HOST_ADDR')
           AND fpov.level_id = 10001;           
        
        ln_profile_option_count := ln_profile_option_count + SQL%ROWCOUNT ;

        dbms_output.put_line('Updating Profile Option Value Of Profile - XXRFS_REMIT_DEFAULT_EMAIL_ADDRESS');
        UPDATE apps.fnd_profile_option_values fpov
        SET fpov.profile_option_value = 'no-reply@tribuspoint.com' 
         WHERE fpov.profile_option_id = (SELECT fpo.profile_option_id
                                         FROM apps.fnd_profile_options fpo
                                         WHERE fpo.profile_option_name = 'XXRFS_REMIT_DEFAULT_EMAIL_ADDRESS')
           AND fpov.level_id = 10001;           
        
        ln_profile_option_count := ln_profile_option_count + SQL%ROWCOUNT ;
        
        COMMIT;
        dbms_output.put_line('Updation of profile options is successfully completed');
        dbms_output.put_line( 'Total No Of profile options updated - ' || ln_profile_option_count );
        EXCEPTION
        WHEN OTHERS THEN
        ROLLBACK;
        dbms_output.put_line('Exception while updating profile options');
        dbms_output.put_line( 'Total No Of profile options updated - ' || ln_profile_option_count );
    END update_profile_options;

PROCEDURE refresh_efsr_ozf_sys_param
    IS
    ln_sys_param_count   NUMBER :=0;
    BEGIN
        dbms_output.put_line('Executing refresh_efsr_ozf_sys_param()');
        
        dbms_output.put_line('Deleting records from EFSR_OZF_SYS_PARAMETERS .. ');
        DELETE FROM efsr.efsr_ozf_sys_parameters WHERE TRX_TYPE IN ('CUST','VEND','ITEM','POS','STK','CLAIM','CHECK','ACCR','PAY','PAYIN','ACCRIN','AUTOPAY','ACCRVAL','PPV') ;         
        dbms_output.put_line('Total No of records deleted from EFSR_OZF_SYS_PARAMETERS - ' || SQL%ROWCOUNT);
        
        dbms_output.put_line('Inserting records into EFSR_OZF_SYS_PARAMETERS .. ');
        
        INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('CUST'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_IFACE_CUST_DIR' ,
            '/u001/oracle/TESTP/db_intf/customersdata/in' ,
            '/u001/oracle/TESTP/db_intf/customersdata/out'      ,
            '/u001/oracle/TESTP/db_intf/customersdata/archive' ,
            '35.160.191.236'   ,
                'Y',
            'EFSR_CUST_IFACE_STG'  ,
            'EFSR_CUST_IFACE_ARCH' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_CUST_IFACE_PKG.cust_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYY/MM/DD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Customer Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('VEND'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_VEND_DIR' ,
            '/u001/oracle/TESTP/db_intf/vendorsdata/in' ,
            '/u001/oracle/TESTP/db_intf/vendorsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/vendorsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_VEND_IFACE_STG'  ,
            'EFSR_VEND_IFACE_ARCH' ,
            'EFSR',
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_VEND_IFACE_PKG.vend_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYY/MM/DD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Vendor Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('ITEM'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_ITEM_DIR' ,
            '/u001/oracle/TESTP/db_intf/itemsdata/in' ,
            '/u001/oracle/TESTP/db_intf/itemsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/itemsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_ITEM_IFACE_STG'  ,
            'EFSR_ITEM_IFACE_ARCH' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_ITEM_IFACE_PKG.item_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYY/MM/DD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Item Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('POS'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_POS_DIR' ,
            '/u001/oracle/TESTP/db_intf/salesdata/in' ,
            '/u001/oracle/TESTP/db_intf/salesdata/out'      ,
            '/u001/oracle/TESTP/db_intf/salesdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_POS_IFACE_STG'  ,
            'EFSR_OZF_POS_IFACE_ARCH' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_OZF_POS_IFACE_PKG.pos_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has POS Sales Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;


    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('STK'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_STK_DIR' ,
            '/u001/oracle/TESTP/db_intf/stockreceiptsdata/in' ,
            '/u001/oracle/TESTP/db_intf/stockreceiptsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/stockreceiptsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_STK_IFACE_STG'  ,
            'EFSR_OZF_STK_IFACE_ARCH' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_OZF_STK_IFACE_PKG.stk_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Stock Register Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('CLAIM'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_CLAIM_DIR' ,
            '/u001/oracle/TESTP/db_intf/adjustmentsdata/in' ,
            '/u001/oracle/TESTP/db_intf/adjustmentsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/adjustmentsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_ADJ_IFACE_STG'  ,
            'EFSR_OZF_ADJ_IFACE_ARCH' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_OZF_CLAIM_IFACE_PKG.claim_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYY/MM/DD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Claims Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('CHECK'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_CHECK_DIR' ,
            '/u001/oracle/TESTP/db_intf/checksdata/in' ,
            '/u001/oracle/TESTP/db_intf/checksdata/out'      ,
            '/u001/oracle/TESTP/db_intf/checksdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_CHECK_IFACE_STG'  ,
            'EFSR_OZF_CHECK_IFACE_ARCH' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_OZF_CHECK_IFACE_PKG.check_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'MM/DD/YYYY',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Checks Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('ACCR'              ,
                'OUT',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_ACCR_DIR' ,
            '/u001/oracle/TESTP/db_intf/accrualsdata/in' ,
            '/u001/oracle/TESTP/db_intf/accrualsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/accrualsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_ACCR_IFACE_STG'  ,
            '' ,
            'EFSR',
            'EFSR_IFACE_TRX_OUTBOUND_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.process_outbound_datafiles' ,
            'EFSR_OZF_ACCR_IFACE_PKG.process_accr_batch' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Accrual Outbound Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('PAY'              ,
                'OUT',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_PAY_DIR' ,
            '/u001/oracle/TESTP/db_intf/paymentsdata/in' ,
            '/u001/oracle/TESTP/db_intf/paymentsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/paymentsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_PAY_IFACE_STG'  ,
            '' ,
            'EFSR',
            'EFSR_IFACE_TRX_OUTBOUND_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.process_outbound_datafiles' ,
            'EFSR_OZF_PAY_IFACE_PKG.process_payment_batch' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Payment Outbound Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('PAYIN'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_PAY_DIR' ,
            '/u001/oracle/TESTP/db_intf/paymentsdata/in' ,
            '/u001/oracle/TESTP/db_intf/paymentsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/paymentsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_PAY_IFACE_STG'  ,
            '' ,
            'EFSR',
            'EFSR_IFACE_TRX_INBOUND_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_OZF_PAY_IFACE_PKG.process_payment_batch_in' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Payment Inbound Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('ACCRIN'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_ACCR_DIR' ,
            '/u001/oracle/TESTP/db_intf/accrualsdata/in' ,
            '/u001/oracle/TESTP/db_intf/accrualsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/accrualsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_ACCR_IFACE_STG'  ,
            '' ,
            'EFSR',
            'EFSR_IFACE_TRX_INBOUND_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_OZF_ACCR_IFACE_PKG.process_accr_batch_in' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Accrual Inbound Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('AUTOPAY'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_AUTOPY_DIR' ,
            '/u001/oracle/TESTP/db_intf/autopaydata/in' ,
            '/u001/oracle/TESTP/db_intf/autopaydata/out'      ,
            '/u001/oracle/TESTP/db_intf/autopaydata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_AUTOPAY_IFACE_STG'  ,
            'EFSR_AUTOPAY_IFACE_ARCH' ,
            'EFSR',
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.process_outbound_datafiles' ,
            'EFSR_AUTOPAY_IFACE_PKG.autopay_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Autopay Inbound Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('ACCRVAL'              ,
                'OUT',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_ACCRVL_DIR' ,
            '/u001/oracle/TESTP/db_intf/valuationdata/in' ,
            '/u001/oracle/TESTP/db_intf/valuationdata/out'      ,
            '/u001/oracle/TESTP/db_intf/valuationdata/archive' ,
            '35.160.191.236'   ,
                'Y',
            'EFSR_OZF_ACCR_VAL_STG'  ,
            '' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_OUTBOUND_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.process_outbound_datafiles' ,
            'EFSR_OZF_ACCR_IFACE_PKG.validation_accr_batch' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Accrual Validation Outbound Data Interface Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

    INSERT INTO efsr_ozf_sys_parameters
               (trx_type            ,
            in_out_bound        ,
            data_file_delim     ,
            data_file_type      ,
            data_file_dir_db    ,
            server_file_dir_in  ,
            server_file_dir_out ,
            server_file_dir_arch,
            data_file_server_ip ,
            include_header_rec  ,
            stage_table         ,
            archive_table       ,
            cc_appl_name        ,
            cc_process          ,
            api_file_process    ,
            api_batch_process   ,
            api_gen_status_files,
            date_format         ,
            last_data_xfer_time ,
            process_mode        ,
            process_status      ,
            process_comments    )
         VALUES 
               ('STK'              ,
                'IN',
            'P'                 ,
            '.dat'              ,
            'EFSR_OZF_IFACE_PPV_DIR' ,
            '/u001/oracle/TESTP/db_intf/stockreceiptsdata/in' ,
            '/u001/oracle/TESTP/db_intf/stockreceiptsdata/out'      ,
            '/u001/oracle/TESTP/db_intf/stockreceiptsdata/archive' ,
            '35.160.191.236'   ,
            'Y',
            'EFSR_OZF_STK_IFACE_STG'  ,
            'EFSR_OZF_STK_IFACE_ARCH' ,
            'EFSR'  ,
            'EFSR_IFACE_TRX_BATCH_PROCESS' ,
            'EFSR_IFACE_UTIL_PKG.data_file_process' ,
            'EFSR_OZF_STK_IFACE_PKG.stk_batch_process' ,
            'EFSR_IFACE_UTIL_PKG.generate_status_files' ,
                'YYYYMMDD',
                 SYSDATE - 1000,
            'BATCH'             ,
            'S'                 ,
            'This Record Has Purchase Price Variance Related Meta Data' ) ;
        ln_sys_param_count := ln_sys_param_count + SQL%ROWCOUNT ;

        COMMIT;
        dbms_output.put_line('Refresh of EFSR_OZF_SYS_PARAMETERS is successfully completed');
        dbms_output.put_line( 'Total No Of records inserted into EFSR_OZF_SYS_PARAMETERS - ' || ln_sys_param_count );
        EXCEPTION
        WHEN OTHERS THEN
        ROLLBACK;
        dbms_output.put_line('Exception while refreshing table EFSR_OZF_SYS_PARAMETERS');
        dbms_output.put_line( 'Total No Of records inserted into EFSR_OZF_SYS_PARAMETERS -' || ln_sys_param_count );
    END refresh_efsr_ozf_sys_param;


BEGIN 
        dbms_output.put_line('Execution of rfs_post_clone_testp begins..');

        dbms_output.put_line('Calling refresh_efsr_ozf_sys_param() to refresh table - EFSR_OZF_SYS_PARAMETERS');
        refresh_efsr_ozf_sys_param;
        dbms_output.put_line('Successfully refreshed table - EFSR_OZF_SYS_PARAMETERS');

        dbms_output.put_line('Calling update_ftp_parameters() to update lookup_type - EFSR_FTP_PARAMETERS');
        update_ftp_parameters;
        dbms_output.put_line('Successfully updated lookup_type - EFSR_FTP_PARAMETERS');

        dbms_output.put_line('Calling update_xref_rfs_to_ebs_lt() to update lookup_type - EFSR_XFER_RFS_TO_EBS_PARAMETER');
        update_xref_rfs_to_ebs_lt;
        dbms_output.put_line('Successfully updated lookup_type - EFSR_XFER_RFS_TO_EBS_PARAMETER');

        dbms_output.put_line('Calling update_profile_options() to update profile options');
        update_profile_options;
        dbms_output.put_line('Successfully updated profile options');

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    dbms_output.put_line('Exception in Main block of rfs_post_clone_testp() ');    
END rfs_post_clone_testp;
/
EXIT;
/